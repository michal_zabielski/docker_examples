Examples
----------

W katalogu apache_nginx odpalasz:

```
docker-compose build
docker-compose up
```

i smiga.

Jak chcesz odpalic w tle to robisz:
```
docker-compose up -d
```

Żeby zatrzymać robisz:
```
docker-compose down
```
Żeby skasować:
```
docker-compose rm
```
